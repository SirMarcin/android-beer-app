package pl.piwnym.piwnymokiem;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class BeerListFragment extends ListFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    String[] beerNames;

    public static BeerListFragment newInstance() {
        BeerListFragment fragment = new BeerListFragment();
        Bundle args = new Bundle();
        //args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        beerNames = getResources().getStringArray(R.array.beers);
        ArrayAdapter<String> beerNamesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, beerNames);

        setListAdapter(beerNamesAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Snackbar.make(v,"Otwieram podgląd dla " + beerNames[position], Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }
}
