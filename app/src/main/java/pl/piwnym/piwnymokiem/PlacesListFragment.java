package pl.piwnym.piwnymokiem;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PlacesListFragment extends ListFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    String[] placeNames;

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        placeNames = getResources().getStringArray(R.array.places);
        ArrayAdapter<String> placesNamesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, placeNames);

        setListAdapter(placesNamesAdapter);
    }

    public static PlacesListFragment newInstance() {
        PlacesListFragment fragment = new PlacesListFragment();
        Bundle args = new Bundle();
        //args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        //fragment.setArguments(args);
        return fragment;
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Snackbar.make(v, "Otwieram podgląd dla " + placeNames[position], Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

}
